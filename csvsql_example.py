#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
Name:           csvsql_example.py
Description:    example implementation of csvsql.py
Usage:          ./csvsql_example.py test.csv
Author:         Ralf Hersel - ralf.hersel@rum3ber.ch
License:        GPL3
Repository:		https://codeberg.org/ralfhersel/csvsql
Date:           2021.12.13
Version:        0.03
'''

# === Import ===================================================================

from csvsql import db


# === Main =====================================================================

def main(args):
	csv_filename = args[1]														# pass the CSV-file as argument
	data = db(csv_filename, reload=True)										# load csv into database
	print(data.get_header())													# column names
	sql = f"SELECT * | WHERE Name LIKE 'Al%'"
	rows = data.query(sql, type='list', key=0)									# default type=list
	print(len(rows), 'records')													# get amount of records
	for row in rows:
		print(row)
	data.close()																# close database
	return 0

if __name__ == '__main__':
	import sys
	sys.exit(main(sys.argv))
