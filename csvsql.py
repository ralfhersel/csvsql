#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''
Name:           csvsql.py
Description:    read csv file into sqlite db
Usage:          from csvsql import cs
Author:         Ralf Hersel - ralf.hersel@rum3ber.ch
License:        GPL3
Repository:		https://codeberg.org/ralfhersel/csvsql
Date:           2021.12.13
Version:        0.03
'''

# === Import ===================================================================

import csv
import sqlite3
import os


# === Features =================================================================

class db:
	
	def __init__(self, csv_file, reload=True):									# read csv, create db, insert into db
		db_exists = os.path.exists('csvsql.db')									# does db exist?
		
		csvfile = open(csv_file)
		dialect = csv.Sniffer().sniff(csvfile.read(1024))						# detect csv format
		csvfile.seek(0)															# set on first record
		self.contents = csv.reader(csvfile, dialect)							# read csv
		self.header = next(self.contents)										# get first row
		self.header = [''.join(e for e in string if e.isalnum()) for string in self.header]	# sanitize header

		self.connection = sqlite3.connect('csvsql.db')							# open database
		self.cursor = self.connection.cursor()

		if reload or not db_exists:												# load csv
			sql = 'DROP TABLE IF EXISTS csvsql'									# delete table if exists
			self.cursor.execute(sql)
		
			sql = 'CREATE TABLE csvsql ('										# create table csvsql
			for column in self.header:
				sql += column + ' TEXT, '
			sql = sql[:-2] + ')'
			print(sql)
			self.cursor.execute(sql)
		
			sql = 'INSERT INTO csvsql ('										# insert all rows from csv
			for column in self.header:
				sql += column + ', '
			sql = sql[:-2] + ') VALUES('
			sql += '?, ' * len(self.header)
			sql = sql[:-2] + ')'
			self.cursor.executemany(sql, self.contents)
			
			self.connection.commit()
			csvfile.close()


	def get_header(self):														# table column names
		return self.header


	def query(self, sql, type='list', key=0):									# get rows from sql, tpye = list/dict/set
		sql = sql.replace('|', 'FROM csvsql')
		rows = self.cursor.execute(sql).fetchall()
		if type == 'dict':
			key_col = list(map(list, zip(*rows)))[key]							# transpose and get key row
			rows = dict(zip(key_col, rows))										# convert list to dictionary; key = index column
		elif type == 'set':
			rows = set(rows)													# convert list to set
		return rows


	def close(self):															# close db connection
		self.connection.close()
